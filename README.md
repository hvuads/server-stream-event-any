# React stream event any

## Get started
```
bun a react-stream-event-any
```
```
npm i react-stream-event-any
yarn add react-stream-event-any
pnpm add react-stream-event-any
```

## Usage
```ts
import ServerStreamEvent from "react-stream-event-any"

type StreamData = string

const response = ServerStreamEvent<StreamData>({
    url: `${ApiEndpoint}/ai/chat/completions/${id}`,
    method: "POST",
    header: {
        "Content-Type": "application/json"
    },
    body: JSON.stringify(data)
})

for await (const data of response){
    console.log(data) // type StreamData
}
```