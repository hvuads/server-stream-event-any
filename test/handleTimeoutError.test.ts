import { describe, it, expect } from "bun:test"
import { setTimeout as setTimeoutPs } from "timers/promises"
async function* a() {
    while (true) {
        yield 1
        setTimeout(() => {
            throw new Error("")
        }, 3_000)
    }
}

describe("", () => {
    it("Normal throw error in try catch ", () => {
        let a = 0
        try {
            throw "xxx"
            a = 1
        } catch (error) {
            a = 2
            console.log("error:", error)
        }
        expect(a).toBe(2)
    })

    it("Delay error by settimeout can't catch", () => {
        let a = 0
        try {
            a = 1
            setTimeout(() => {
                throw ""
            }, 3_000)
        } catch (error) {
            a = 2
            console.log("error:", error)
        }
        expect(a).toBe(1)
    })

    it("Delay error by settimeout with Promise can't catch", () => {
        let a = 0
        try {
            a = 1
            new Promise((_, rej) =>
                setTimeout(() => {
                    rej("")
                }, 3_000)
            )
        } catch (error) {
            a = 2
            console.log("error:", error)
        }
        expect(a).toBe(1)
    })

    it("Delay error by settimeout with Promise in the new function can't catch", () => {
        let a = 0
        try {
            function throwError() {
                throw ""
            }
            a = 1
            new Promise((_, rej) =>
                setTimeout(() => {
                    throwError()
                }, 3_000)
            )
        } catch (error) {
            a = 2
            console.log("error:", error)
        }
        expect(a).toBe(1)
    })

    it("Delay error by settimeout with Promise in the new function can't catch and then throw can't catch", () => {
        let a = 0
        try {
            function throwError() {
                throw ""
            }
            a = 1
            new Promise((_, rej) =>
                setTimeout(() => {
                    throwError()
                }, 3_000)
            ).then((e) => {
                throw e
            })
        } catch (error) {
            a = 2
            console.log("error:", error)
        }
        expect(a).toBe(1)
    })

    it("Delay error in try/catch block ", () => {
        let a = 0
        let b = 0
        try {
            a = 1
            setTimeoutPs(300, () => {
                throw "1"
            })
            b = 1
        } catch (error) {
            a = 2
            console.log("error:", error)
        }
        expect(a).toBe(1)
    })
})
