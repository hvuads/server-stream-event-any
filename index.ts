export const sleep = (ms: number) => new Promise((resolve) => setTimeout(resolve, ms))
type StreamEvent<T> = {
    done: boolean
    value: T | string | undefined
}
interface CustomEventProps<T> {
    onDone?: (event?: StreamEvent<T>) => void
    onStream?: (event: StreamEvent<T>) => void
}
export type ServerStreamEventProps<T, MT = "GET" | "POST" | "PUT" | "PATCH" | "DELETE"> = {
    url: string
    header?: HeadersInit
    method: MT
    body: MT extends "GET" ? undefined : Record<string, unknown> | BodyInit | null | undefined
    abort?: AbortController
} & CustomEventProps<T>
export type ServerStreamEventOpts = {
    timeout_sec?: number
    onError?: (errMsg: string) => void
    abort?: AbortController
}
export async function ServerStreamEvent<T>(
    {
        url,
        header,
        method,
        body,
        abort = new AbortController(),
        onStream,
        onDone
    }: ServerStreamEventProps<T>,
    opts: ServerStreamEventOpts = {}
) {
    const response = await fetch(url, {
        method,
        headers: header,
        body: typeof body === "object" ? JSON.stringify(body) : body,
        signal: abort?.signal
    })

    if (!response.ok) throw response

    function genStream() {
        return streamReader<T>(response.body?.getReader(), { abort, ...opts })
    }

    async function readStream() {
        for await (const { done, value } of genStream()) {
            done ? onDone?.() : onStream?.({ done, value })
        }
    }

    if (onDone || onStream) readStream()

    return genStream()
}
let TO: ReturnType<typeof setTimeout>

async function* streamReader<T>(
    reader?: ReadableStreamDefaultReader<Uint8Array>,
    { timeout_sec = 30, onError, abort }: ServerStreamEventOpts = {}
) {
    function setTO() {
        clearTimeout(TO)
        TO = setTimeout(() => {
            if (onError) return onError("Error timeout waiting for response from server.")
            abort?.abort()
        }, timeout_sec * 1000)
    }
    if (!reader) throw new Error("Reader not found.")

    while (true) {
        setTO()
        const { done, value } = await reader.read()
        if (done) {
            clearTimeout(TO)
            return { done, value }
        }

        const res_text = new TextDecoder().decode(value)
        const results = res_text
            .split("\n")
            .filter((v) => v.startsWith("data: "))
            .map((v) => v.substring(6))
            .map<T>(safeJSON)
        for (const result of results) {
            yield { done, value: result }
        }
    }
}

export function safeJSON(txt: string) {
    try {
        return JSON.parse(txt)
    } catch (error) {
        return undefined
    }
}

export default ServerStreamEvent
